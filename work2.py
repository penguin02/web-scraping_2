import sqlite3
import requests
import re
import pandas as pd
import urllib.request
from bs4 import UnicodeDammit

headers={
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36"
    }

#用urllib.request方法访问服务器
def getHtml(page):
    url = "https://9.push2.eastmoney.com/api/qt/clist/get?cb=jQuery1124025500369212952667_1634094365855&pn="+page+"&pz=20&po=1&np=1&ut=bd1d9ddb04089700cf9c27f6f7426281&fltt=2&invt=2&fid=f3&fs=m:0+t:6,m:0+t:80,m:1+t:2,m:1+t:23&fields=f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f12,f13,f14,f15,f16,f17,f18,f20,f21,f23,f24,f25,f22,f11,f62,f128,f136,f115,f152&_=1634094365856"
    req = urllib.request.Request(url, headers=headers)
    html=urllib.request.urlopen(req)
    html=html.read()
    dammit=UnicodeDammit(html,["utf-8","gbk"])
    html=dammit.unicode_markup
    return html

#使用正则表达式获取股票数据
def parsePage(html):
    datas = []
    #股票代码
    findID = re.compile('"f12":"(.*?)",')
    id = re.findall(findID, html)
    #股票名称
    findName = re.compile('"f14":"(.*?)",')
    name = re.findall(findName, html)
    # 最新价
    findPrice = re.compile('"f2":(.*?),')
    price = re.findall(findPrice, html)
    # 涨跌幅
    findChangeRate = re.compile('"f3":(.*?),')
    changeRate = re.findall(findChangeRate, html)
    # 涨跌额
    findChange = re.compile('"f4":(.*?),')
    change = re.findall(findChange, html)
    # 成交额
    findCurrentPrice = re.compile('"f6":(.*?),')
    currentPrice = re.findall(findCurrentPrice, html)
    # 最高
    findMax = re.compile('"f15":(.*?),')
    max = re.findall(findMax, html)
    # 最低
    findMin = re.compile('"f16":(.*?),')
    min = re.findall(findMin, html)

    for i in range(len(id)):
        datas.append([id[i],name[i],price[i],changeRate[i]+"%",change[i],currentPrice[i]+"元",max[i],min[i]])
    return datas

# 按格式打印数据
def printList(list):
	tply="{0:^4}\t{1:^8}\t{2:^8}\t{3:^5}\t{4:^6}\t{5:^5}\t{6:^9}\t{7:^5}\t{8:^5}"
	print(tply.format("序号","股票代码","股票名称","最新价","涨跌幅","涨跌额","成交额","最高","最低",chr(12288)))
	count=1
	for data in list:
		print(tply.format(count,data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7],chr(12288)))
		count = count + 1


# 保存数据到数据库
def saveData2DB(datalist, dbpath):
    init_db(dbpath)
    conn = sqlite3.connect(dbpath)
    cur = conn.cursor()
    for data in datalist:
        for index in range(len(data)):
            data[index] = '"' + data[index] + '"'
            sql = '''
                insert into shares(
                    id,name,price,changeRate,change,currentPrice,max,min
                    )
                    values(%s)
            ''' % ",".join(data)
        cur.execute(sql)
        conn.commit()
    cur.close()
    conn.close()
    print("保存到数据库成功！")


#数据库初始化
def init_db(dbpath):
        sql = '''
            create table shares
            (
            id text,
            name text,
            price text,
            changeRate text,
            change text,
            currentPrice text,
            max text,
            min text
            );
        '''
        conn = sqlite3.connect(dbpath)
        cursor = conn.cursor()
        cursor.execute(sql)
        conn.commit()
        conn.close()


def main():
    html = getHtml("1")     # 爬取第一页的数据
    list = parsePage(html)      # 解析html页面得到所需数据
    printList(list)             # 按格式打印出得到的数据
    dbpath = r'D:\wqwDownload\PyCharm Community Edition 2021.2.2\PycharmProjects\wqwcodes\爬虫实践课\第二次作业\shares.db'  # 数据库路径
    saveData2DB(list, dbpath)       # 将数据保存到数据库


if __name__ =="__main__":
    main()